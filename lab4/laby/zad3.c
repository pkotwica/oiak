#include <stdio.h>


int main() {

char znaki[] = "XYZ123\0";
long int klucz = 27;


printf("%s\n", znaki);


asm(

	"movq $-1, %%r8\n"
	"petla_po_znakach:\n"
	"movq $0, %%rax\n"
	"inc %%r8\n"
	"movb (%0, %%r8, 1), %%al\n"
	"cmp $0, %%rax\n"
	"je koniec\n"
	"cmp $'A', %%rax\n"
	"jl petla_po_znakach\n"
	"cmp $'Z', %%rax\n"
	"jg petla_po_znakach\n"
	"movq %%rax, %%rbx\n"
	"add %1, %%rax\n"
	"cmp $'Z', %%rax\n"
	"jle nie_przekracza\n"
	"sub $'Z', %%rax\n"
	"movq $0, %%rdx\n"
	"movq $26, %%r9\n"
	"div %%r9\n"
	"cmp $0, %%rdx\n"
	"jne jest_reszta\n"
	"movq $26, %%rdx\n"
	"jest_reszta:\n"
	"movq %%rdx, %%rax\n"
	"add $64, %%rax\n"
	"movq %%rax, %%rbx\n"
	"nie_przekracza:\n"
	"movq %%rbx, %%rax\n"
	"movb %%al, (%0, %%r8, 1)\n"
	"jmp petla_po_znakach\n"
	"koniec:\n"

:
:"r"(znaki), "r"(klucz)
:"%rax","%rdx","%rbx","%r8","%r9"
);


printf("%s\n", znaki);



return 0;

}
