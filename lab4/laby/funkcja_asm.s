.text

.global funkcja

.type funkcja, @function


funkcja: 
#rdi -> pierwszy parametr (wskaznik na znak)
#rsi -> drugi parametr (klucz)

movq $-1, %r8 # -> iterator


petla_po_znakach:

movq $0, %rax
inc %r8
movb (%rdi, %r8, 1), %al

cmp $0, %rax
je koniec

cmp $'A', %rax
jl petla_po_znakach

cmp $'Z', %rax
jg petla_po_znakach

add %rsi, %rax
movq %rax, %rbx

cmp $'Z', %rax
jle nie_przekracza

sub $'Z', %rax

movq $0, %rdx
movq $26, %r9
div %r9

cmp $0, %rdx
jne jest_reszta
movq $26, %rdx

jest_reszta:

movq %rdx, %rax
add $64, %rax
movq %rax, %rbx


nie_przekracza:
movq %rbx, %rax
movb %al, (%rdi, %r8, 1)

jmp petla_po_znakach
koniec:
ret











