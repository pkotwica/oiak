.data
SYS_EXIT=60
EXIT_SUCCES=0
SYS_READ=0
STDIN=0
STDOUT=1
SYS_WRITE=1
FILE_OPEN=2
FILE_CLOSE=3
BUFLEN=1024


#kom_start: .ascii "Podaj liczbe: "
#kom_start_ln = .-kom_start

#kom_err: .ascii "Nieprawidlowy ciag znakow!\n"
#kom_err_ln = .-kom_err



file1: .ascii "liczba1.txt\0"
file2: .ascii "liczba2.txt\0"


.bss
.comm txtin1, 1024
.comm txtin2, 1024


.text
.global main
main:


#------------------zerowanie buforow--------------------



#----------wczytanie z pliku ------------------------------
#otwarcie pliku

movq $FILE_OPEN, %rax
movq $file1, %rdi
movq $STDIN, %rsi
movq $0644, %rdx
syscall

movq %rax, %r8  #<----deskryptor

#odczyt pliku

movq $SYS_READ, %rax
movq %r8, %rdi
movq $txtin1, %rsi
movq $BUFLEN, %rdx
syscall

movq %rax, %r10	

#zamkniecie pliku

movq $FILE_CLOSE, %rax
movq %r8, %rdi




#----------------------------zapis do pliku--------------------------

movq $FILE_OPEN, %rax
movq $file2, %rdi
movq $01101, %rsi
movq $0644, %rdx
syscall
movq %rax, %r8



movq $SYS_WRITE, %rax
movq %r8, %rdi
movq $txtin1, %rsi
movq $BUFLEN, %rdx
syscall

#zamkniecie pliku

movq $FILE_CLOSE, %rax
movq %r8, %rdi





#------------------koniec prog-------------------------------

movq $SYS_EXIT, %rax
movq $EXIT_SUCCES, %rdi 
syscall


