.data
SYS_EXIT=60
EXIT_SUCCES=0
SYS_READ=0
STDIN=0
STDOUT=1
SYS_WRITE=1
FILE_OPEN=2
FILE_CLOSE=3
BUFLEN=1024


#kom_start: .ascii "Podaj liczbe: "
#kom_start_ln = .-kom_start

#kom_err: .ascii "Nieprawidlowy ciag znakow!\n"
#kom_err_ln = .-kom_err



file1: .ascii "liczba1.txt\0"
file2: .ascii "liczba2.txt\0"


.bss
.comm txtin1, 1024
.comm txtin2, 1024
.comm liczba1, 1024
.comm liczba2, 1024


.text
.global main
main:


#------------------zerowanie buforow--------------------

movq $BUFLEN, %rdx
movb $0, %al

zerowanieTXT1:
dec %rdx
movb %al, txtin1(, %rdx, 1)
cmp $0, %rdx
jge zerowanieTXT1



movq $BUFLEN, %rdx
movb $0, %al

zerowanieTXT2:
dec %rdx
movb %al, txtin2(, %rdx, 1)
cmp $0, %rdx
jge zerowanieTXT2




movq $BUFLEN, %rdx
movb $0, %al

zerowanieLICZ1:
dec %rdx
movb %al, liczba1(, %rdx, 1)
cmp $0, %rdx
jge zerowanieLICZ1



movq $BUFLEN, %rdx
movb $0, %al

zerowanieLICZ2:
dec %rdx
movb %al, liczba1(, %rdx, 1)
cmp $0, %rdx
jge zerowanieLICZ2

kon_zer:

#----------wczytanie z pliku ------------------------------
#otwarcie pliku

movq $FILE_OPEN, %rax
movq $file1, %rdi
movq $STDIN, %rsi
movq $0644, %rdx
syscall

movq %rax, %r8  #<----deskryptor

#odczyt pliku

movq $SYS_READ, %rax
movq %r8, %rdi
movq $txtin1, %rsi
movq $BUFLEN, %rdx
syscall

movq %rax, %r10	#<-------ilosc wczytanych znakow
#dec %r10

#zamkniecie pliku

movq $FILE_CLOSE, %rax
movq %r8, %rdi


#--------------------------zamiana na lcizbe w pamieci--------------
#-----liczba1---------

#movq $0, %rax
#movq txtin1(,%rax,8), %rdx

movq $0, %rdx
movq $4, %rsi
movq %r10, %rax
dec %rax
dec %r10
div %rsi

movq %rax, %rsi	#<---wynik dzielenia
movq %rdx, %rdi #<---reszta z dzielenia

movq $-1, %r15




little1:
movq $0, %rax
movq $0, %rbx
movq $0, %rcx
movq $0, %rdx

cmp $0, %rsi
jle koniec_little1

inc %r15

dec %r10
movb txtin1(, %r10, 1), %al
sub $'0', %rax

dec %r10
movb txtin1(, %r10, 1), %bl
sub $'0', %rbx

shl $2, %rbx
or %rbx, %rax

dec %r10
movb txtin1(, %r10, 1), %cl
sub $'0', %rcx

shl $4, %rcx
or %rcx, %rax

dec %r10
movb txtin1(, %r10, 1), %dl
sub $'0', %rdx

shl $6, %rdx
or %rdx, %rax



movb %al, liczba1(, %r15, 1)
dec %rsi
jmp little1

koniec_little1:


#---dopsianie pozostalego bajtu o ile istnieje---

movq $0, %rax
movq $0, %rbx
movq $0, %rcx

cmp $0, %rdi
je brak_dodatkowy1



dodatkowy1:
dec %r10
movb txtin1(, %r10, 1), %al
sub $'0', %rax

cmp $1, %rdi
je brak1

dec %r10
movb txtin1(, %r10, 1), %bl
sub $'0', %rbx
shl $2, %rbx
or %rbx, %rax

cmp $2, %rdi
je brak1

dec %r10
movb txtin1(, %r10, 1), %cl
sub $'0', %rcx
shl $4, %rcx
or %rcx, %rax


brak1:
inc %r15
movb %al, liczba1(, %r15, 1)


brak_dodatkowy1:



push %r15	#<--------------- zapisanie ilosci bajtow z ilu sklada sie liczba1



#----------wczytanie z pliku ------------------------------
#otwarcie pliku

movq $FILE_OPEN, %rax
movq $file2, %rdi
movq $STDIN, %rsi
movq $0644, %rdx
syscall

movq %rax, %r8  #<----deskryptor

#odczyt pliku

movq $SYS_READ, %rax
movq %r8, %rdi
movq $txtin2, %rsi
movq $BUFLEN, %rdx
syscall

movq %rax, %r10	#<-------ilosc wczytanych znakow
#dec %r10

#zamkniecie pliku

movq $FILE_CLOSE, %rax
movq %r8, %rdi

break:

#--------------------------zamiana na lcizbe w pamieci--------------
#-----liczba2---------

#movq $0, %rax
#movq txtin1(,%rax,8), %rdx

movq $0, %rdx
movq $4, %rsi
movq %r10, %rax
dec %rax
dec %r10
div %rsi

movq %rax, %rsi	#<---wynik dzielenia
movq %rdx, %rdi #<---reszta z dzielenia

movq $-1, %r15




little2:
movq $0, %rax
movq $0, %rbx
movq $0, %rcx
movq $0, %rdx

cmp $0, %rsi
jle koniec_little2

inc %r15

dec %r10
movb txtin2(, %r10, 1), %al
sub $'0', %rax

dec %r10
movb txtin2(, %r10, 1), %bl
sub $'0', %rbx

shl $2, %rbx
or %rbx, %rax

dec %r10
movb txtin2(, %r10, 1), %cl
sub $'0', %rcx

shl $4, %rcx
or %rcx, %rax

dec %r10
movb txtin2(, %r10, 1), %dl
sub $'0', %rdx

shl $6, %rdx
or %rdx, %rax



movb %al, liczba2(, %r15, 1)
dec %rsi
jmp little2

koniec_little2:


#---dopsianie pozostalego bajtu o ile istnieje---

movq $0, %rax
movq $0, %rbx
movq $0, %rcx

cmp $0, %rdi
je brak_dodatkowy2



dodatkowy2:
dec %r10
movb txtin2(, %r10, 1), %al
sub $'0', %rax

cmp $1, %rdi
je brak2

dec %r10
movb txtin2(, %r10, 1), %bl
sub $'0', %rbx
shl $2, %rbx
or %rbx, %rax

cmp $2, %rdi
je brak2

dec %r10
movb txtin2(, %r10, 1), %cl
sub $'0', %rcx
shl $4, %rcx
or %rcx, %rax


brak2:
inc %r15
movb %al, liczba2(, %r15, 1)


brak_dodatkowy2:

push %r15 	#<---------------- zapisanie z ilu bajtow skalda sie liczba2


kon_little:
#----------------------------------dodawanie-----------------------------

movq $0, %rdi

clc
pushfq

dodawanie:

movq liczba1(, %rdi, 8), %rax
movq liczba2(, %rdi, 8), %rbx


popfq
adc %rax, %rbx
pushfq

movq %rbx, txtin1(, %rdi, 8)

inc %rdi
cmp $128, %rdi
jne dodawanie

popfq
clc

kon_dod:



#---------------------------------konwersja wyniku--------------------


#-----przygotowanie (z ilu bajtow sklada sie liczba)-----

pop %rax	#<----- sciaganie ze stosu ilosci bajtow z ktorych skaldaja sie dodwane liczby
pop %rbx

cmp %rax, %rbx
jle no_save_bigger

movq %rbx, %rax

no_save_bigger:

inc %rax


#okreslenie ile nalezy razy brac 3 bajty


















#----------------------------zapis do pliku--------------------------

#movq $FILE_OPEN, %rax
#movq $file2, %rdi
#movq $01101, %rsi
#movq $0644, %rdx
#syscall
#movq %rax, %r8



#movq $SYS_WRITE, %rax
#movq %r8, %rdi
#movq $txtin1, %rsi
#movq $BUFLEN, %rdx
#syscall

#zamkniecie pliku

#movq $FILE_CLOSE, %rax
#movq %r8, %rdi





#------------------koniec prog-------------------------------

movq $SYS_EXIT, %rax
movq $EXIT_SUCCES, %rdi 
syscall


