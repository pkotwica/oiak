.data
SYS_EXIT=60
EXIT_SUCCES=0
SYS_READ=0
STDIN=0
STDOUT=1
SYS_WRITE=1
BUFLEN=512


kom_start: .ascii "Podaj liczbe: "
kom_start_ln = .-kom_start

kom_err: .ascii "Nieprawidlowy ciag znakow!\n"
kom_err_ln = .-kom_err


.bss
.comm txtin, 512
.comm txtout, 512


.text
.global main
main:


#------------------wprowadzanie tekstu----------------------
wprowadz_liczbe:

movq $SYS_WRITE, %rax
movq $STDOUT, %rdi
movq $kom_start, %rsi
movq $kom_start_ln, %rdx
syscall


movq $SYS_READ, %rax
movq $STDIN, %rdi
movq $txtin, %rsi
movq $BUFLEN, %rdx
syscall

movq $0, %rsi
movb txtin(,%rsi,1), %bl

#------------sprawdzenie poprawnosci ciagu znakow---------
movq %rax, %rdx
dec %rdx

movq $0, %rsi

sprawdzanie_znaku:
cmp %rdx, %rsi
je koniec_sprawdzania

movb txtin(,%rsi,1), %bl

cmp $48, %bl
jl err

cmp $57, %bl
jg err

inc %rsi
jmp sprawdzanie_znaku


err:
movq $SYS_WRITE, %rax
movq $STDOUT, %rdi
movq $kom_err, %rsi
movq $kom_err_ln, %rdx
syscall
jmp wprowadz_liczbe



koniec_sprawdzania:



#-------------zamina na wartosc w rejestrze------------------
movq %rax, %rdi		#przypomnienie i zapamietanie ilosci wprowadzonych znakow
dec %rdi
#movq %rax, %r10
movq $0, %r14		#wynikowa liczba
movq $0, %r8

zamiana:
movq %r14, %rax
movq $10, %rsi
mul %rsi
movq %rax, %r14

movq $0, %rbx
movb txtin(,%r8,1), %bl

sub $48, %rbx

add %rbx, %r14

inc %r8
cmp %rdi, %r8
jne zamiana

koniec_zamiany:




#-----------------zamiana na system dziewiatkowy-------------



TODO:





#----------------------wyswietlanie wynikowego tekstu-----------
#movq $SYS_WRITE, %rax
#movq $STDOUT, %rdi
#movq $txtin, %rsi
#movq $BUFLEN, %rdx
#syscall






movq $SYS_EXIT, %rax
movq $EXIT_SUCCES, %rdi 
syscall


