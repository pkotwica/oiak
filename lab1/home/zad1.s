.data

SYS_EXIT=60
EXIT_SUCCES=0
SYS_READ=0
STDIN=0
STDOUT=1
SYS_WRITE=1
BUFLEN=512

kom_start: .ascii "Podaj tekst: "
kom_start_ln = .-kom_start

.bss
.comm txtin, 512
.comm txtout, 512


.text
.global main

main:


#------------------wprowadzanie tekstu----------------------
movq $SYS_WRITE, %rax
movq $STDOUT, %rdi
movq $kom_start, %rsi
movq $kom_start_ln, %rdx
syscall


movq $SYS_READ, %rax
movq $STDIN, %rdi
movq $txtin, %rsi
movq $BUFLEN, %rdx
syscall


#----------------------przeksztalcanie tekstu-------------------



#dec %rax

petla:
dec %rax
movb txtin(,%rax,1), %bl

cmp $0, %bl
je koniec

cmp $58, %bl
jl liczba

cmp $64, %bl
jg litera



liczba:
cmp $48, %bl
jl petla

add $5, %bl

movb %bl, txtin(,%rax,1)

jmp petla



litera:
cmp $90, %bl
jg petla

add $3, %bl

movb %bl, txtin(,%rax,1)


jmp petla
koniec:




#----------------------wyswietlanie wynikowego tekstu-----------
movq $SYS_WRITE, %rax
movq $STDOUT, %rdi
movq $txtin, %rsi
movq $BUFLEN, %rdx
syscall






movq $SYS_EXIT, %rax
movq $EXIT_SUCCES, %rdi 
syscall


