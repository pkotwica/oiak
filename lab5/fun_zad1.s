.text

jedynka: .double 1.0
minus_jedynka: .double -1.0

.global arctg

.type arctg, @function


arctg:
push %rsp
mov %rsp, %rbp

#rdi -> ilosc krokow
#mmx0 -> argument x

#argumenty przez stos
sub $8, %rsp
movsd %xmm0, (%rsp)
fldl (%rsp)
fwait


#zaadowanie wyniku
fld %st

#zaladowanie mianownika
fld1


fld %st(2)
fwait

#s0 -> licznik
#s1 -> mianownik
#s2 -> wynik
#s3 -> x


#przygotowanie licznika
#fmull minus_jedynka


petla:
dec %rdi
cmp $0, %rdi
jle koniec


#wprowadzenie pomocniczego rejestru
fld %st

#pomnozenie licznika przez x oraz -1
fmul %st(4), %st(0)
fmul %st(4), %st(0)
fmull minus_jedynka

#inkrementacja mianownika
fxch %st(2)
faddl jedynka
faddl jedynka
fxch %st(2)


#zapamietanie nowego licznika
fst %st(1)

#dzielenie licznik/mianownik
fdiv %st(2), %st(0)


#zapamietanie noewgo wyniku
fadd %st(0), %st(3)

#zdjecie ze stosu niepotrzebnej zmiennej
fstp (%rsp)




jmp petla
koniec:

fwait
fxch %st(2)
fstpl (%rsp)
movsd (%rsp), %xmm0



mov %rbp, %rsp
pop %rbp
ret
