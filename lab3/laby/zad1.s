.data

SYS_EXIT=60
EXIT_SUCCES=0
SYS_READ=0
STDIN=0
STDOUT=1
SYS_WRITE=1
BUFOR=1024



komunikat1: .ascii "Wprowadz ciag znakow: "
dl_komunikat1 = .-komunikat1

komunikat2: .ascii "Pierwsze wystapienie ciagu: "
dl_komunikat2 = .-komunikat2


komunikat3: .ascii "Brak takiego ciagu: "
dl_komunikat3 = .-komunikat3


.bss
.comm txtin, BUFOR
.comm txtout, 20
.comm text, 8

.text
.global main

main:

#-----------------------wprowadzenie ciagu znakow-------------------------

movq $SYS_WRITE, %rax
movq $STDOUT, %rdi
movq $komunikat1, %rsi
movq $dl_komunikat1, %rdx
syscall

movq $SYS_READ, %rax
movq $STDIN, %rdi
movq $txtin, %rsi
movq $BUFOR, %rdx
syscall
  


#----------------pzrygotowanie args i wywolanie func----------------

movq %rax, %r8	#<---- ilosc wczytanych znakow
movq $txtin, %rax	#<---- miejsce w pamieci ciagu
movq $4, %rbx


call funkcja_ciagX

#rax - zwracana pozycja ciagu lub liczba ujemna jezeli brak
#--------------przygotwanie odpowiedzi na ekran---------------

cmp $0, %rax
jge jest


#---brak-----
movq $SYS_WRITE, %rax
movq $STDOUT, %rdi
movq $komunikat3, %rsi
movq $dl_komunikat3, %rdx
syscall

jmp koniec_prog


#----jest----
jest:


movq %rax, %r10
movq $0, %r8
movq $0, %rbx
movq $19, %r9

movq $SYS_WRITE, %rax
movq $STDOUT, %rdi
movq $komunikat2, %rsi
movq $dl_komunikat2, %rdx
syscall

movq $'\n', txtout(, %r9, 1)
dec %r9


movq %r10, %rax
dzielenie:
movq $10, %rdi

movq $0, %rdx
div %rdi

add $'0', %rdx
movb %dl, txtout(, %r9, 1)
dec %r9

cmp $0, %rax
jg dzielenie


movq $SYS_WRITE, %rax
movq $STDOUT, %rdi
movq $txtout, %rsi
movq $20, %rdx
syscall





#--------koniec programu------------

koniec_prog:
movq $SYS_EXIT, %rax
movq $EXIT_SUCCES, %rdi 
syscall




#-----------------------------fynkcje---------------------------------------



funkcja_ciagX:
#rax - ciag
#rbx - ilosc wystapien x
#r8 - dlugosc ciagu (ze znakiem zerowym)

movq %rax, %rdi
movq $-1, %rdx
movq $0, %rax
movq $0, %r9


petla_po_znakach:
cmp %rbx, %r9
je koniec_jest

inc %rdx
cmp %r8, %rdx
jge koniec_brak

movb (%rdi, %rdx, 1), %al


cmp $'x', %al
jne brak
inc %r9
jmp petla_po_znakach

brak:
movq $0, %r9
jmp petla_po_znakach



koniec_brak:
movq $-1, %rax
jmp koniec_funkcji


koniec_jest:
movq %rdx, %rax
sub %rbx, %rax
add $1, %rax

koniec_funkcji:
ret























