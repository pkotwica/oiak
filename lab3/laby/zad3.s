.data

SYS_EXIT=60
EXIT_SUCCES=0
SYS_READ=0
STDIN=0
STDOUT=1
SYS_WRITE=1
BUFOR=1024
BUFOROUT=40



komunikat1: .ascii "f(n) = f(n-2) - 2f(n-3)\nf(0) = 2\nf(1) = 1\nf(2) = 3\nPodaj n: "
dl_komunikat1 = .-komunikat1

komunikat2: .ascii "f(n) = "
dl_komunikat2 = .-komunikat2


.bss
.comm txtin, BUFOR
.comm txtout, BUFOROUT

.text
.global main

main:

#-----------------------wprowadzenie ciagu znakow-------------------------

movq $SYS_WRITE, %rax
movq $STDOUT, %rdi
movq $komunikat1, %rsi
movq $dl_komunikat1, %rdx
syscall

movq $SYS_READ, %rax
movq $STDIN, %rdi
movq $txtin, %rsi
movq $BUFOR, %rdx
syscall
  
movq %rax, %r8
dec %r8

#-----------zamiana wprowadzonej liczby na liczbe w rejestrze-----
dec %r8
movq $1, %rbx
movq $0, %r10 	#<-----------LICZBA


zamiana_int:
movq $0, %rax
cmp $0, %r8
jl koniec_int

movb txtin(, %r8, 1), %al
sub $'0', %rax

mul %rbx
add %rax, %r10

movq $10, %rax
mul %rbx
movq %rax, %rbx

dec %r8
jmp zamiana_int
koniec_int:



#----------------pzrygotowanie args i wywolanie funca---------------
break1:
movq %r10, %rax
call funkcja_rekurencyjna


#rax - liczba wynikowa
#--------------przygotwanie odpowiedzi na ekran---------------
break2:

movq %rax, %r10
movq $1, %rcx 	#czy ujemna

cmp $0, %r10
jge wysw


movq $-1, %rax
mul %r10
movq %rax, %r10

movq $-1, %rcx
push %rcx

wysw:
movq $SYS_WRITE, %rax
movq $STDOUT, %rdi
movq $komunikat2, %rsi
movq $dl_komunikat2, %rdx
syscall


movq $0, %r8
movq $0, %rbx
movq $BUFOROUT, %r9
dec %r9


movq $'\n', txtout(, %r9, 1)
dec %r9


movq %r10, %rax
dzielenie:
movq $10, %rdi

movq $0, %rdx
div %rdi

add $'0', %rdx
movb %dl, txtout(, %r9, 1)
dec %r9

cmp $0, %rax
jg dzielenie



pop %rcx
jne koniec_wysw

movq $'-', %rax
movb %al, txtout(, %r9, 1)



koniec_wysw:
movq $SYS_WRITE, %rax
movq $STDOUT, %rdi
movq $txtout, %rsi
movq $BUFOROUT, %rdx
syscall





#--------koniec programu------------

koniec_prog:
movq $SYS_EXIT, %rax
movq $EXIT_SUCCES, %rdi 
syscall




#-----------------------------fynkcje---------------------------------------



funkcja_rekurencyjna:
#rax <-----argument funkcji




cmp $0, %rax
jne nie_zero
movq $2, %rax
jmp koniec_funkcji

nie_zero:
cmp $1, %rax
jne nie_jeden
movq $1, %rax
jmp koniec_funkcji


nie_jeden:
cmp $2, %rax
jne nie_dwa
movq $3, %rax
jmp koniec_funkcji

nie_dwa:



#-------rekurencja------------
movq %rax, %r8
push %r8


sub $2, %r8

movq %r8, %rax
call funkcja_rekurencyjna


pop %r8
push %rax


movq %r8, %rax
sub $3, %rax
call funkcja_rekurencyjna
movq %rax, %rdx


#----koniec------
pop %rcx
movq $2, %rax
mul %rdx

sub %rax, %rcx

movq %rcx, %rax


koniec_funkcji:
ret























